<?php
/**
 * @file
 * ns_business_i18n.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function ns_business_i18n_strongarm() {
  $export = array();

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_ns_business_case';
  $strongarm->value = '2';
  $export['language_content_type_ns_business_case'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_ns_business_news';
  $strongarm->value = '2';
  $export['language_content_type_ns_business_news'] = $strongarm;

  return $export;
}
