<?php
/**
 * @file
 * ns_business_news.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ns_business_news_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function ns_business_news_node_info() {
  $items = array(
    'ns_business_news' => array(
      'name' => t('News'),
      'base' => 'node_content',
      'description' => t('Let\'s you create news articles.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
