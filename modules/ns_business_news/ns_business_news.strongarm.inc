<?php
/**
 * @file
 * ns_business_news.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function ns_business_news_strongarm() {
  $export = array();

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_ns_business_news';
  $strongarm->value = array();
  $export['menu_options_ns_business_news'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_ns_business_news';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_ns_business_news'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_ns_business_news';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_ns_business_news'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_ns_business_news';
  $strongarm->value = '1';
  $export['node_preview_ns_business_news'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_ns_business_news';
  $strongarm->value = 1;
  $export['node_submitted_ns_business_news'] = $strongarm;

  return $export;
}
