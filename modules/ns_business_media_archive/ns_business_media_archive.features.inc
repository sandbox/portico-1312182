<?php
/**
 * @file
 * ns_business_media_archive.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ns_business_media_archive_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function ns_business_media_archive_node_info() {
  $items = array(
    'ns_business_media_archive' => array(
      'name' => t('Media archive'),
      'base' => 'node_content',
      'description' => t('Media Archive content allows for creating PR related content with attached files that for example journalists can use for articles the write about the organization.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
