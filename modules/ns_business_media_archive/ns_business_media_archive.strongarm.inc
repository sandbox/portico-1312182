<?php
/**
 * @file
 * ns_business_media_archive.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function ns_business_media_archive_strongarm() {
  $export = array();

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_ns_business_media_archive';
  $strongarm->value = '0';
  $export['language_content_type_ns_business_media_archive'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_ns_business_media_archive';
  $strongarm->value = array(
    0 => 'main-menu',
  );
  $export['menu_options_ns_business_media_archive'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_ns_business_media_archive';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_ns_business_media_archive'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_ns_business_media_archive';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_ns_business_media_archive'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_ns_business_media_archive';
  $strongarm->value = '1';
  $export['node_preview_ns_business_media_archive'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_ns_business_media_archive';
  $strongarm->value = 0;
  $export['node_submitted_ns_business_media_archive'] = $strongarm;

  return $export;
}
