<?php
/**
 * @file
 * ns_business_case.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ns_business_case_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function ns_business_case_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}

/**
 * Implements hook_node_info().
 */
function ns_business_case_node_info() {
  $items = array(
    'ns_business_case' => array(
      'name' => t('Case'),
      'base' => 'node_content',
      'description' => t('A business case that can be displayed on the site.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
