; $Id$

; API
api = 2

; Core
core = 7.x

; Contrib
projects[i18n][version] = 1.1

; Unstable modules
projects[field_collection][type] = module
projects[field_collection][download][type] = git
projects[field_collection][download][url] = http://git.drupal.org/project/field_collection.git
projects[field_collection][download][revision] = a2214e9d7c194b49d8a07fd45990d28347feb17d

; Themes
